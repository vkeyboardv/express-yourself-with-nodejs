# stage-2-express-yourself-with-nodejs

Simple RESTful API using Node.js, Express, fs-extra, express-async-await etc.
Heroku: [https://express-yourself-with-node-js.herokuapp.com/](https://express-yourself-with-node-js.herokuapp.com/)

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install stage-2-express-yourself-with-nodejs.

```bash
npm i
```

## Usage

```bash
npm start
```
Go to [http://localhost:3000](http://localhost:3000)

## Requests
| HTTP Method  | Description |
| ------------- | ------------- |
| GET: /user  | Get array of all users  |
| GET: /user/:id  | Get user by id  |
| POST: /user  | Create user. Example body: ```{"first_name":"POST","last_name":"TEST","email":"test@test.com","gender":"Male", "ip_adress": "0.0.0.0"}```
   |
| PUT: /user:id  | Update user. Example body: ```{"id": 1005, "first_name":"PUT","last_name":"TEST","email":"test@test.com","gender":"Male", "ip_adress": "0.0.0.0"}```
  |
| DELETE: /user/:id  | Delete user by id  |

Do not forget about authentication. 
Key: Authorization, Value: admin

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
