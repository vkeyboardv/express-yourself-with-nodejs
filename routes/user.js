const express = require('express');
const router = express.Router();

const fs = require('fs-extra');

const { isAuthorized } = require('../middlewares/auth.middleware');

require('express-async-await')(router);

router.get('/', isAuthorized, async (req, res) => {
  try {
    const users = await fs.readJson('userlist.json');

    res.send(users);
  } catch (err) {
    console.error(err);
  }
});

router.get('/:id', isAuthorized, async (req, res) => {
  const id = req.params.id;

  try {
    const users = await fs.readJson('userlist.json');

    let user = null;

    for (let i = 0; i < users.length; i++) {
      if (users[i].id == id) {
        user = users[i];
        break;
      }
    }

    if (user) {
      res.send(user);
    } else {
      res.status(404).send();
    }
  } catch (err) {
    console.error(err);
  }
});

router.post('/', isAuthorized, async (req, res) => {
  if (!req.body) {
    return res.sendStatus(400);
  }

  try {
    const users = await fs.readJson('userlist.json');

    const { first_name, last_name, email, gender, ip_address } = req.body;

    const id = Math.max.apply(Math, users.map(o => {
      return o.id;
    }))

    const user = {
      id: id + 1,
      first_name: first_name,
      last_name: last_name,
      email: email,
      gender: gender,
      ip_address: ip_address,
      last_change_ip: req.ip.split(':').pop()
    };

    users.push(user);

    await fs.writeJson('userlist.json', users);
    res.send(user);
  } catch (err) {
    console.error(err);
  }
});

router.put('/', isAuthorized, async (req, res) => {
  if (!req.body) {
    return res.sendStatus(400);
  }

  try {
    const users = await fs.readJson('userlist.json');

    const { id, first_name, last_name, email, gender, ip_address } = req.body;

    let user;
    for (let i = 0; i < users.length; i++) {
      if (users[i].id == id) {
        user = users[i];
        break;
      }
    }

    if (user) {
      user.first_name = first_name;
      user.last_name = last_name;
      user.email = email;
      user.gender = gender;
      user.ip_address = ip_address;
      user.last_change_ip = req.ip.split(':').pop();

      await fs.writeJson('userlist.json', users);
      res.send(user);

    } else {
      res.status(404).send(user);
    }
  } catch (err) {
    console.error(err);
  }
});

router.delete('/:id', isAuthorized, async (req, res) => {
  const id = req.params.id;

  try {
    const users = await fs.readJson('userlist.json');

    let index = -1;

    for (let i = 0; i < users.length; i++) {
      if (users[i].id == id) {
        index = i;
        break;
      }
    }

    if (index > -1) {
      const user = users.splice(index, 1)[0];
      await fs.writeJson('userlist.json', users);
      res.send(user);
    } else {
      res.status(404).send();
    }
  } catch (err) {
    console.error(err);
  }
});

module.exports = router;
